@echo off
set busy=1
color 1F

:start
echo.
echo.
echo " ________         _________        __________                     ___             __   "
echo "  \_____  \        \_   ___ \       \______   \_______  ____   __| _/_ __   _____/  |_ "
echo "   /   |   \       /    \  \/        |     ___/\_  __ \/  _ \ / __ |  |  \_/ ___\   __\"
echo "  /    |    \      \     \____       |    |     |  | \(  <_> ) /_/ |  |  /\  \___|  |  "
echo "  \_______  /  /\   \______  /  /\   |____|     |__|   \____/\____ |____/  \___  >__|  "
echo "          \/   \/          \/   \/                                \/           \/      "
echo.
echo.
echo ========================================================
echo OPERATION CORE  - Product... MoveToLocationHandler  - %busy%
pause
set Destination="C:\projects\OperationCore-out-MoveToLocationHandler\"

mkdir %Destination%

cd %Destination%
taskkill /F /IM E5-OCS.exe
del *.* /s /f /q

echo OperationCore...
xcopy "c:\projects\evita.operationcore\Aline.OperationCore.Services.AccessControl\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

xcopy "C:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

echo Configuration...
xcopy "C:\projects\scripts\MyConfigs\OperationCore\*" "%Destination%" /Y /q

copy "C:\projects\scripts\MyConfigs\OperationCore\MoveToLocation.nlog.config" "%Destination%\nlog.config"

start E5-OCS.exe MoveToLocation

set /A busy = busy + 1
goto start
