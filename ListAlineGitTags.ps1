# Import-Module posh-git
Set-Alias ssh "$env:ProgramFiles\git\usr\bin\ssh.exe"
Set-Alias ssh-agent "$env:ProgramFiles\git\usr\bin\ssh-agent.exe"
Set-Alias ssh-add "$env:ProgramFiles\git\usr\bin\ssh-add.exe"
Start-SshAgent -Quiet

$repositories = 
"C:\projects\epia", 
"C:\projects\aline",
"C:\projects\hostinterface",
"C:\projects\aline.ordermanager", 
"C:\projects\Kiosk", 
"C:\projects\evita.operationcore"
# "C:\projects\tov",
# "C:\projects\vls-ghent",
# "C:\projects\nynas-ge"

# ssh-add '/c/Users/jan.verley/.ssh/id_rsa'

Write-Output "# Git Tags"

$now = Get-Date -Format g
Write-Output "Generated on $now"

foreach ($repository in $repositories) 
{
  $repoName = Split-Path $repository -Leaf
  Write-Output "## $repoName"
  Write-Output "`n"
  Push-Location -Path $repository

  # git fetch --progress "--all" -q

  git log --tags="Aline_*" --no-walk --date=iso-local --pretty='- %<(45,trunc)%d %C(auto)%h %cd'
  # $tag = git tag -l "Aline_*"

  # $tags = $tag.split(" ")

  # $tags | foreach {
  #   git show $_ --oneline
  #   Write-Output "- $info"
  # }

  Write-Output "`n"
  
  Pop-Location
}

