param (
    [Parameter(Mandatory=$true)][String]$command,
    [string]$filename,
    [string]$sqlInstance=".\MSSQLSERVER2016",
    [string]$username="sa",
    [string]$password="",
    [string]$databaseName="MegaVitaDevGen",
    [string]$executionFolder="",
    [switch]$forceMigrate=$false
)
$sonnectionString = "data source=$sqlInstance;initial catalog=$databaseName;integrated security=true"

Import-Module SqlPs -DisableNameChecking
Invoke-SqlCmd -Query "IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE (name = '$databaseName')))
BEGIN
    USE master;
    ALTER DATABASE $databaseName SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE $databaseName ;
END
USE [master]
GO" -ErrorAction 'Stop' -Verbose -QueryTimeout 1800 -ConnectionString "$connectionString" 

write-host "Dropped $databaseName" 

Push-Location "..\..\\service"

.\migrate.exe Agidens.Terminal.Suite.SqlStore.dll /startUpConfigurationFile="Agidens.Terminal.Suite.SqlStore.dll.config" /connectionProviderName="System.Data.SqlClient" /connectionString=$connectionString

write-host "Migrated $databaseName" 

Isvoke-SqlCmd -InputFile "%teamcity.build.checkoutDir%\scripts\00_InsertMasterData.sql" -Database $databaseName -ServerInstance "$sqlInstance" -ErrorAction 'Stop' -Verbose -QueryTimeout 1800 -Username "%env.SQLUSER%" -Password "%env.SQLPASS%"

write-host "##teamcity[message text='=== Inserted Master Data in $databaseName .']" 
Isvoke-SqlCmd -InputFile "%teamcity.build.checkoutDir%\scripts\01_InsertTestData.sql" -Database $databaseName -ServerInstance "$sqlInstance" -ErrorAction 'Stop' -Verbose -QueryTimeout 1800 -Username "%env.SQLUSER%" -Password "%env.SQLPASS%"

write-host "##teamcity[message text='=== Inserted Test Data in $databaseName .']" 


# Pop-Location