@echo off

set Source-Epia="C:\projects\Epia\"

set Destination="C:\projects\OrderManagerServer-out\"

mkdir "%Destination%"
mkdir "%Destination%\Data"
mkdir "%Destination%\Data\ProfileCatalog"

set busy=1

:start
echo %~f0
echo Start OrderManager - Server %busy%

cd "%Source-OM%"

git branch

xcopy "%Source-Epia%bin\Any CPU\Debug" "%Destination%" /s /q /f /d /e /r /y /k

ECHO configs...
xcopy "c:\projects\scripts\MyConfigs\Portal\ProfileCatalog.xml" "%Destination%\Data\ProfileCatalog" /s /f /e /r /y /k
xcopy "c:\projects\scripts\MyConfigs\Portal\Egemin.Epia.Server.exe.config" "%Destination%" /s /f /e /r /y /k

::start epia server
cd %Destination%
start Egemin.Epia.Server.exe

pause

set /A busy = busy + 1
goto start