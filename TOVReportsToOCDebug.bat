@echo off
:start
echo Start Copying TOV Reporting libs to OC Debug folder...
pause
set Destination="c:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\"
set Source="c:\projects\tov\TOV\TOV.OperationCore.Services.Printing\bin\Debug\"

xcopy "%Source%\*" "%Destination%" /s /q /f /d /e /r /y /k

goto start