@echo off

set Source-BE="C:\projects\aline\"
set Source-OM="C:\projects\aline.ordermanager\"
set Source-Epia="C:\projects\Epia\"

set Destination="C:\projects\OrderManager-out\"

set busy=1

:start
echo %~f0
echo Start OrderManager - %busy%
echo. 
echo. 
echo  #######            ##     ##        
echo ##     ##           ###   ###        
echo ##     ##           #### ####        
echo ##     ##           ## ### ##        
echo ##     ##           ##     ##        
echo ##     ##    ###    ##     ##    ### 
echo  #######     ###    ##     ##    ### 
echo. 
echo. 

echo ========================================================
pause

taskkill /F /IM Egemin.Epia.Shell.exe

rmdir /s /q "%Destination%"

:: copy Epia assemblies
xcopy "%Source-Epia%bin\Any CPU\Debug" "%Destination%" /s /q /f /d /e /r /y /k

:: copy 
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.Data\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.Services\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Resources\*" "%Destination%\Resources" /s /q /f /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\bin\Debug\Data\Resources\*" "%Destination%\Data\Resources" /s /q /f /e /r /y /k
xcopy "%Source-OM%3-Modules\Aline.OrderManager.Core.UiComponents\Data\Security\*" "%Destination%\Data\Security" /s /q /f /e /r /y /k

::echo Contract Management...
::xcopy "%Source-OM%3-Modules\Aline.Contract.UiComponents\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k

echo Timesheets...
xcopy "%Source-OM%3-Modules\Aline.TimeSheet.UIComponents\bin\Debug\*.dll" "%Destination%"  /s /q /f /d /e /r /y /k


:: copy assemblies shared with Aline Back-End 
REM xcopy "%Source-BE%Agidens.MessageBroker.Msg\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
REM xcopy "%Source-BE%Aline.Resource.Service\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k
REM xcopy "%Source-BE%Agidens.Terminal.Suite.Service\bin\Debug\*.dll" "%Destination%" /s /q /f /d /e /r /y /k


xcopy "c:\projects\scripts\MyConfigs\Portal\ProfileCatalog.xml" "%Destination%\Data\ProfileCatalog" /s /f /e /r /y /k
xcopy "c:\projects\scripts\MyConfigs\Portal\Egemin.Epia.Shell.exe.config" "%Destination%" /s /f /e /r /y /k
REM xcopy "c:\projects\tov\Configuration\Local\Portal\*.*" "%Destination%" /s /f /e /r /y /k

REM echo TestReports1...
REM xcopy "c:\projects\TestReports\TestReports1\bin\Debug\*.dll" "%Destination%" /q /s /f /e /r /y /k /c
REM xcopy "c:\projects\TestReports\TestReports1\bin\Debug\Data\Resources\*" "%Destination%\Data\Resources" /s /q /f /e /r /y /k

::start epia server and shell
cd %Destination%
REM start Egemin.Epia.Server.exe
start Egemin.Epia.Shell.exe

set /A busy = busy + 1
goto start