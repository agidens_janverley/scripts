Import-Module "C:\projects\scripts\MarkDownToPdf.dll"

Get-ChildItem "C:\projects\Aline.wiki" -Recurse | 
Where-Object {$_.Extension -eq ".md"} | 
Foreach-Object {
  $_.FullName
  Get-Content -Path $_.FullName |Out-String| ConvertFrom-MarkDownFile | New-Item $_.FullName.Replace('.md', '.pdf')
}