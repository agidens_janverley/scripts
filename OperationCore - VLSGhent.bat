@echo off
set busy=1

:start
echo. 
echo. 
echo       #######           #####        
echo       #     #          #     #       
echo       #     #          #             
echo       #     #          #             
echo       #     #    ###   #         ### 
echo       #     #    ###   #     #   ### 
echo       #######    ###    #####    ### 
echo. 
echo. 
echo ========================================================
echo OPERATION CORE  - With VLS Ghent libs...   - %busy%
pause
set Destination="C:\projects\OperationCore-out\"

cd %Destination%
taskkill /F /IM E5-OCS.exe
del *.* /s /f /q

xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing.DefaultReports\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

xcopy "C:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

REM xcopy "c:\projects\tov\TOV\TOV.OperationCore.Services.Printing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

xcopy "c:\projects\vls-ghent\Configuration\Local\04 Operation services\*" "%Destination%" /s /f /r /y /k

echo logo...

xcopy "c:\projects\vls-ghent\Vls.Ghent\PrintingServices\Logo\logo.png" "%Destination%\Logo\" /s /f /r /y /k
REM /f /r /p /s /y

start E5-OCS.exe PrintServer

set /A busy = busy + 1
goto start