param 
(
    [parameter(HelpMessage="Contract Management -Executes sqls related to Contract Management")]
    [alias("ContractManagement")]
    [Switch]
    $cm,

    [parameter(HelpMessage="No Data - Skips the Basic Data sql '01_Basic_Configuration.sql' ")]
    [alias("NoData")]
    [Switch]
    $nd

)
Import-Module SqlPs -DisableNameChecking

new-object System.String('-', 50)
"Re-Creating EvitaDev db:"

Get-ChildItem "C:\projects\Aline\1 Deployment\1 Datastore\" | 
Where-Object {$_.Extension -eq ".sql"} | 
Where-Object {!$_.Name.Contains("01_Basic_Configuration") -or !$nd } | 
Where-Object {!$_.Name.Contains("ContractManagement") -or $cm } | 
Foreach-Object {
    $_.FullName

    Invoke-Sqlcmd -InputFile $_.FullName -ServerInstance .\MSSQLSERVER2016 
}

new-object System.String('-', 50)
"MigrationsV2:"

Get-ChildItem "C:\projects\Aline\1 Deployment\1 Datastore\MigrationsV2" | Where-Object {$_.Extension -eq ".sql"} | Sort-Object -Property FullName |
Foreach-Object {
    $_.FullName
    Invoke-Sqlcmd -InputFile $_.FullName -ServerInstance .\MSSQLSERVER2016 
}

new-object System.String('-', 50)
"Done!"