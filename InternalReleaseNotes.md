# Release Notes Aline

Source: Youtrack Aline [tag: For1.16](http://10.10.14.20:8081/issues/AL?q=tag%3A%20For1.16)  
Generated on 07/12/2018 10:07:59

## Bug (32)

- [AL-1406](http://10.10.14.20:8081/issue/AL-1406) - As driver I want 'bulkhead' to be translated to 'baffleplate' on Kiosk
- [AL-1642](http://10.10.14.20:8081/issue/AL-1642) - Aline Portal - Sender: No translation found
- [AL-1655](http://10.10.14.20:8081/issue/AL-1655) - As warehouse manager I want all tank shell reference temperatures to be <> null  
  **Tank Shell reference temperature is not mandatrory field. Existing tanks are migrated to contains by default 288.15K**
- [AL-1670](http://10.10.14.20:8081/issue/AL-1670) - Tank VCF calculation uses BOL figures instead of tank figures  
  **Parameters from tank are used instead.**
- [AL-1672](http://10.10.14.20:8081/issue/AL-1672) - Linear calculation not correct  
  **Linear VCF calculation correctly implemented as `VCF =  Dobs / Dref` - `VCF = ( Dref - ( ( Tobs - Tref ) * DCF ) )  / Dref`**
- [AL-1694](http://10.10.14.20:8081/issue/AL-1694) - As External ERP, I want to have flexfields exported automatically on WO close ...  
  **Work order entry flex fields were accidentally returned instead of the flex fields of the sales order entry linked to the work order entry.**
- [AL-1701](http://10.10.14.20:8081/issue/AL-1701) - ERP import for sales order. Error management is not working correctly + SO can be created without SOE
- [AL-1711](http://10.10.14.20:8081/issue/AL-1711) - Exception on detach WOE
- [AL-1724](http://10.10.14.20:8081/issue/AL-1724) - Missing translation for the new report Truck Turn Around Times  
  **Translations added to db for portal nodes.**
- [AL-1725](http://10.10.14.20:8081/issue/AL-1725) - Sample report printing
- [AL-1727](http://10.10.14.20:8081/issue/AL-1727) - Truck loading configuration - assigned quantity issue and 36°C unknown origin
- [AL-1729](http://10.10.14.20:8081/issue/AL-1729) - Add Source equipment to a WOE  
  ***before:*![](equipmentNotOrdered.PNG)**
- [AL-1732](http://10.10.14.20:8081/issue/AL-1732) - Double click on row grid does not open details view anymore
- [AL-1736](http://10.10.14.20:8081/issue/AL-1736) - ERP - Flex fields are not imported anymore ...  
  **Fixed invalid use of non-navigation property in linq to entities.**
- [AL-1742](http://10.10.14.20:8081/issue/AL-1742) - Ship transfer - in case of missing quality result the system throws an exception
- [AL-1744](http://10.10.14.20:8081/issue/AL-1744) - ERP WorkOrder export uses wrong WOE qty field  
  **The ERP export exports the the calculated qantity when not using a weighbridge.**
- [AL-1747](http://10.10.14.20:8081/issue/AL-1747) - Baffle plate selection is only updated for trailers  
  **The Driver has to explicitly confirm baffleplates on a container, before being able to continue.**
- [AL-1750](http://10.10.14.20:8081/issue/AL-1750) - Log quality result ... Unexpected error  
  **When no Quality Parameter is selected, a dialog is presented to the user.**
- [AL-1754](http://10.10.14.20:8081/issue/AL-1754) - Truck loading configuration report  - not working in case of multi orders  
  **Traversal of work order compartments is moved to its own loop, instead of as a nested inside the work order entry loop.**
- [AL-1758](http://10.10.14.20:8081/issue/AL-1758) - Impossible to start a transfer from kiosk with the 'no weighbridge' feature
- [AL-1759](http://10.10.14.20:8081/issue/AL-1759) - Transfer can be started when the backend is returning 'can not be started' ...  
  **Checks for starting and stopping Transfers are delegated to Aline core.**
- [AL-1761](http://10.10.14.20:8081/issue/AL-1761) - Sample Label Report - Batch number does not match current value  
  **Sample Label report is updated to make the request for tank quality parameters with the current local time.**
- [AL-1772](http://10.10.14.20:8081/issue/AL-1772) - On loading confirmation report , we want to have the product synonym and not the product name.
- [AL-1775](http://10.10.14.20:8081/issue/AL-1775) - When loading on calibrated loading bay, release of WOE can be simultaneously.
- [AL-1778](http://10.10.14.20:8081/issue/AL-1778) - On save measurement on start transfer ship to tank an exception was thrown.
- [AL-1779](http://10.10.14.20:8081/issue/AL-1779) - CTsh is rounded to 16 dec and cubic without systemoptions set where it should be default 5 and linear
- [AL-1781](http://10.10.14.20:8081/issue/AL-1781) - WO Setreadyfortransfer event is triggered on WOE setreadyfortransfer  
  **To be filled in when `To Verify`**
- [AL-1782](http://10.10.14.20:8081/issue/AL-1782) - allow empty strings in Translations
- [AL-1785](http://10.10.14.20:8081/issue/AL-1785) - BackgroundworkerWrapper should get the Context on demand => null exceptions
- [AL-1786](http://10.10.14.20:8081/issue/AL-1786) - print operations instruction throws an exception, sample label , loading confirmation report
- [AL-1790](http://10.10.14.20:8081/issue/AL-1790) - Operations Instruction use rounding in Weights
- [AL-1792](http://10.10.14.20:8081/issue/AL-1792) - make UN number a string in the report  dto

## User Story (2)

- [AL-1499](http://10.10.14.20:8081/issue/AL-1499) - As dispatcher I want to be able to print a 'operations instruction'  - improved Aline standard report!
- [AL-1767](http://10.10.14.20:8081/issue/AL-1767) - Lower and upper limit of loadingboundaries should be nullable

## Business Logic Correction (1)

- [AL-1589](http://10.10.14.20:8081/issue/AL-1589) - As Warehouse manager I want a correct validation message when adding a strapping volume higher then tank capacity

## Enhancement (2)

- [AL-1720](http://10.10.14.20:8081/issue/AL-1720) - As inventory manager I want to be able to calc tank strapping expansion correction based on more than one method so we can comply with the calibration report of the tank.
- [AL-1773](http://10.10.14.20:8081/issue/AL-1773) - sample label should use less inkt and have Synonym  printed
