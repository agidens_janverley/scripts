@echo off
set busy=1
color 1F

:start
echo.
echo.
echo #######         #####         ####### ####### #     # 
echo #     #        #     #           #    #     # #     # 
echo #     #        #                 #    #     # #     # 
echo #     #        #                 #    #     # #     # 
echo #     # ###    #       ###       #    #     #  #   #  
echo #     # ###    #     # ###       #    #     #   # #   
echo ####### ###     #####  ###       #    #######    #    
echo.
echo.
echo ========================================================
echo OPERATION CORE  - With TOV libs...   - %busy%
pause
set Destination="C:\projects\OperationCore-out\"

cd %Destination%
taskkill /F /IM E5-OCS.exe
del *.* /s /f /q

echo OperationCore...
xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing.DefaultReports\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

echo TOV Printing...
xcopy "c:\projects\tov\TOV\Reporting\TOV.Reporting.Printserver\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

echo Configuration...
xcopy "c:\projects\tov\Configuration\Local\04 Operation services\*" "%Destination%" /Y /q

echo logo...

xcopy "C:\projects\tov\TOV\TOV.OperationCore.Services.Printing\Logo\logo.png" "%Destination%\Logo\" /s /f /r /Y /k
REM /f /r /p /s /y

start E5-OCS.exe PrintServer

set /A busy = busy + 1
goto start
