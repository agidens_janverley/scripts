# Release Notes Aline

Source: Youtrack Aline [Fix versions: 1.20.0.0]  
Generated on 04/03/2019 14:07:37

## Business Logic Correction (2)

- [AL-1245] - Loading Ranges for Truck / RTC  
  **Correctly determine degree of filling for RTC and Truck depending on product and container properties.
Does not auto adjust for loading temperature.
4 new Configuraton settings: see wiki
NON_HAZARDOUS_COMPARTMENT_CAPACITY_LITER, NON_HAZARDOUS_LIMIT_IS_PERCENTAGE, NON_HAZARDOUS_LIMIT_VALUE_GT_CAPACITY, NON_HAZARDOUS_LIMIT_VALUE_LT_CAPACITY**

- [AL-1957] - On operation instruction, quantity to load is only shown in LGOV  
  **Added an extra field in the loaded and requested columns of the product movements. And replaced the GOV with GSV**


## Task (5)

- [AL-1595] - As a dispatcher, I would like to see all (functional) event logs on a WO  
  **Automatic events are logged throughout the life cycle of a work order. An extra endpoint for contract `IEventLogManager` needs to be configured and the Ninject configuration of the Aline Core Service needs an extra binding '<bind service="Agidens.Terminal.Suite.Shared.Events.IDispatcher, Agidens.Terminal.Suite.Shared" to="Aline.EventLog.Service.EventLogDispatcher, Aline.EventLog.Service" />'.**

- [AL-1615] - Support for non operational truck related orders  
  **Aline now supports Operational Truck WorkOrder entries. These WOE's are unrelated to any Transfer of Product. The life cycle is simplified to `Open,Deleted,Validated,Aborted and Closed`**

- [AL-1635] - WO location timeline detail report  
  **New base configured report added via seed method.
Added ability for reports to have nested translations and added 2 unit tests for it.
The 2 requested filters are available. Report is database configured so OperationCore needs connection string set.**

- [AL-1950] - Loading the screen for attaching a SOE to a WO takes a long time to load in case of many SOE's  
  **New setting declared in portal: `AutoLoadDataWhenAttachingSalesorder`
When this setting contains a valid boolean (true/false), it will act on the selection screen for attaching a SOE on a WO.
false: do not auto-load SO/SOE
true : auto-load (default)**

- [AL-1954] - As a driver I have entered my birth date but I'm not in the list; what should I do?  
  **A new button is added "I'm not in the list", depending on the setting `CanCreateNewDriver`, the driver add himself, or is shown a message.**


## Enhancement (5)

- [AL-1766] - Time in queue in human readable format on terminal overview  
  **In the Portal the TerminalViewLocationModel's property TimeInLocationInSeconds is now a string representation, formatted to a human readable format.**

- [AL-1830] - As a dispatcher I want to generate/Print a WO Weighing Overview Report from the portal  
  **Added new report category 'Execution' and report 'WeighingOverview' in the database.
Gave 3 parameters to the report which are required for rendering:
- DisplayWOEinfo (boolean)
- OperationalWeighing (dropdownlist)
- WOReference (string)**

- [AL-1857] - As Kiosk I want to be able to spec the type of MoT for which a new layout can be created, instead of All Or None  
  **The MeansOfTransport Kiosk Screen has a new parameter *CanCreateMeansOfTransportTypes* This replaces the existing *CanCreateMeansOfTransport*.
A list of MoT types can be entered. Only those types can be created in the Kiosk flow. See wiki.**

- [AL-1921] - As dipsatcher I want some value fields outlined to the right on add COE dialog  
  **Aligned the content of the AddCOE screen right, where possible.**

- [AL-1949] - Improve filtering queries  
  **Generated queries are simplified: "contains [empty string]" clauses are skipped.**


## Bug (8)

- [AL-1866] - As dispatcher I want the report updated with the CMR counter  
  **The Net and Gross weight on the CMR has new calculations.
Section 1 and 2 the first 2 lines are now the Name and AddressLine1.
ProductSynonym has been added.
Some layout changes were done just to make sure all data is displayed and fitted on the report.**

- [AL-1953] - When detaching a WOE from a WO, an exception is thrown when the WOE has comments  
  **The WOE can be properly deleted, together with comments etc.**

- [AL-1966] - Product is not being imported on clean database - Specified cast is not valid  
  **Densities are interpreted as Density in VAC during import**

- [AL-1973] - Impossible to attach SO/SOE via Kiosk - loading boundaries impact  
  **technical (reference mismatch)**

- [AL-1975] - Maximum tank strapping not allowed.  
  **Corrected the checks on capacity in TankPart now allowing tank strappings up to the tank Capacity.**

- [AL-1977] - Overrule CAQ of transfer is returning an unhandled exception.  
  **Technical bug.**

- [AL-1978] - PersonDto doesn't fill in Person ErpReference
- [AL-1983] - Test Tank to Flexi bag operational instruction

## User Story (1)

- [AL-1915] - As CS I want the SOE comment to be copied into WOE comment at attach SOE on WO  
  **Updated the constructor used by the method AttachSalesOrderEntryAsync to now add the SalesOrderEntryComments to the WorkOrderEntryComments.**

