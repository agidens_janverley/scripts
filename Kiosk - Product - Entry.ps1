$counter = 0

$source = "C:\projects\Kiosk\"
$destinationBase = "C:\projects\Kiosk-out\"

while ($true) {

Write-Output "=============================="
Write-Output "KIOSK - Product - $counter"

$kiosk = "Entry"
Write-Output "$kiosk ..."

$destination = "$destinationBase$kiosk"

Remove-Item $destination -Recurse -ErrorAction Continue

New-Item  $destination -ItemType Directory| Out-Null
New-Item  $destination\Components -ItemType Directory| Out-Null

Copy-Item "$source\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Shell\bin\Debug\*" -Destination "$destination" -Recurse
Copy-Item "$source\Agidens.Aline.Kiosk\Agidens.Aline.Kiosk.Screens\bin\Debug\Agidens.Aline.Kiosk.Screens.*" -Destination "$destination\Components"
Copy-Item "$source\Configuration\Local\$kiosk\*" -Destination "$destination"

# Copy-Item "C:\projects\tov\TOV\TOV.Kiosk\bin\Debug\TOV.Kiosk.dll" -Destination "$destination\Components"

Copy-Item "C:\projects\scripts\photo.jpg" -Destination "$destination\Resources\"
Copy-Item "C:\projects\scripts\NLog.config" -Destination "$destination\"

Push-Location $destination

Start-Process -FilePath Agidens.Aline.Kiosk.Shell.exe

Pop-Location
Pause
Write-Output "Stopping $kiosk ..."

Stop-Process -Name "Agidens.Aline.Kiosk.Shell" -Force -ErrorAction Continue

$counter++
}