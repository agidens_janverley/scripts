$repositories = 
"C:\projects\aline",
"C:\projects\aline.ordermanager", 
"C:\projects\evita.operationcore",
"C:\projects\kiosk"
# "C:\projects\tov",
# "C:\projects\vls-ghent",
# "C:\projects\nynas-ge"

$tag = "Aline_1.23.0.0"

# ssh-add '/c/Users/jan.verley/.ssh/id_rsa'

foreach ($repository in $repositories) {
  $repository
 
Push-Location -Path $repository

git status

git fetch --progress "--all"
git checkout origin/master

git tag $tag

# git branch $newBranch
# git checkout $newBranch
git push origin $tag
# git checkout master

Pop-Location
}

