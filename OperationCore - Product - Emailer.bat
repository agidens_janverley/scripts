@echo off
set busy=1
color 1F

:start
echo.
echo.
echo " ________         _________        __________                     ___             __   "
echo "  \_____  \        \_   ___ \       \______   \_______  ____   __| _/_ __   _____/  |_ "
echo "   /   |   \       /    \  \/        |     ___/\_  __ \/  _ \ / __ |  |  \_/ ___\   __\"
echo "  /    |    \      \     \____       |    |     |  | \(  <_> ) /_/ |  |  /\  \___|  |  "
echo "  \_______  /  /\   \______  /  /\   |____|     |__|   \____/\____ |____/  \___  >__|  "
echo "          \/   \/          \/   \/                                \/           \/      "
echo.
echo.
echo ========================================================
echo OPERATION CORE  - Product...   - %busy%
pause
set Destination="C:\projects\OperationCore-out-mailer\"

mkdir %Destination%

cd %Destination%
taskkill /F /IM E5-OCS.exe
del *.* /s /f /q

echo OperationCore...
xcopy "C:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

xcopy "C:\projects\evita.operationcore\Aline.OperationCore.Services.ERPInterfacing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
REM xcopy "C:\projects\evita.operationcore\Services\Aline.OperationCore.Services.Notification\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k


    echo Configuration...
xcopy "c:\projects\evita.operationcore\Configuration\Local\*" "%Destination%" /Y /q

echo logo...
xcopy "C:\projects\scripts\photo.jpg" "%Destination%\Logo\" /s /f /r /Y /k

start E5-OCS.exe ERPInterfacing
REM start E5-OCS.exe NotificationService

set /A busy = busy + 1
goto start
