@echo off
set busy=1
color 1F

:start
echo.
echo.
echo " ________         _________        __________                     ___             __   "
echo "  \_____  \        \_   ___ \       \______   \_______  ____   __| _/_ __   _____/  |_ "
echo "   /   |   \       /    \  \/        |     ___/\_  __ \/  _ \ / __ |  |  \_/ ___\   __\"
echo "  /    |    \      \     \____       |    |     |  | \(  <_> ) /_/ |  |  /\  \___|  |  "
echo "  \_______  /  /\   \______  /  /\   |____|     |__|   \____/\____ |____/  \___  >__|  "
echo "          \/   \/          \/   \/                                \/           \/      "
echo.
echo.
echo ========================================================
echo OPERATION CORE  - Product...   - %busy%
pause
set Destination="C:\projects\OperationCore-out\"

mkdir %Destination%

cd %Destination%
taskkill /F /IM E5-OCS.exe
del *.* /s /f /q

echo OperationCore...
REM xcopy "C:\projects\evita.operationcore\Aline.OperationCore.Services.ERPInterfacing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Services\Agidens.Aline.OperationCore.Services.Printing.DefaultReports\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k
xcopy "C:\projects\evita.operationcore\Evita.OperationCore\bin\Debug\*" "%Destination%" /s /q /f /d /e /r /y /k

echo Configuration...
REM xcopy "c:\projects\evita.operationcore\Configuration\Local\*" "%Destination%" /Y /q
xcopy "C:\projects\scripts\MyConfigs\OperationCore\*" "%Destination%" /Y /q

echo logo...
xcopy "C:\projects\scripts\photo.jpg" "%Destination%\Logo\" /s /f /r /Y /k

REM start E5-OCS.exe ERPInterfacing
start E5-OCS.exe PrintServer

set /A busy = busy + 1
goto start
